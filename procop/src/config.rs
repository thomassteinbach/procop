use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub enum SourceType {
    Git(String),
    // Http(String),
}

#[derive(Serialize, Deserialize)]
pub enum FileType {
    Plain,
    // Template,
}

#[derive(Serialize, Deserialize)]
pub struct DataSource {
    pub source_type: SourceType,
    pub file_type: FileType,
}

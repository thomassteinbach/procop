use crate::procop;
use directories::ProjectDirs;
use std::error;
use std::fmt::Display;
use xarif_git;

#[derive(Debug, PartialEq)]
pub struct InvalidGitUrl(&'static str);

impl Display for InvalidGitUrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl error::Error for InvalidGitUrl {}

#[derive(Debug)]
pub enum UpdateRepoError {
    ParseUrl(InvalidGitUrl),
    UpdateRepoError(xarif_git::UpdateRepoError),
}

impl UpdateRepoError {
    fn from_parse_url(err: InvalidGitUrl) -> UpdateRepoError {
        UpdateRepoError::ParseUrl(err)
    }

    fn from_update_repo(err: xarif_git::UpdateRepoError) -> UpdateRepoError {
        UpdateRepoError::UpdateRepoError(err)
    }
}

pub fn update_repository(url: &str) -> Result<(), UpdateRepoError> {
    let mut target_path = ProjectDirs::from(
        procop::PROCOP_QUALIFIER,
        procop::PROCOP_ORGANIZATION,
        procop::PROCOP_APPLICATION,
    )
    .expect(
        "\
                No valid home directory path could be retrieved from the operating system. \
                This program wants to store its application data there.",
    )
    .data_dir()
    .to_path_buf();

    target_path.push(git_url_to_filename(url).map_err(UpdateRepoError::from_parse_url)?);

    xarif_git::update_repository(url, target_path.as_path())
        .map_err(UpdateRepoError::from_update_repo)?;

    Ok(())
}

/// Converts a Git URL to a sanitized file name.
///
/// This function takes a Git URL as input and converts it into a sanitized file name
/// that can be used to store the corresponding repository's data. The function internally
/// extracts the repository name using the [`extract_git_repo_name`] function and then sanitizes
/// it using the [`sanitise_file_name::sanitise`] function.
///
/// # Arguments
///
/// * `url`: A string slice representing the Git URL to convert into a file name.
///
/// # Returns
///
/// - `Ok(String)`: If the Git URL is valid and the repository name can be extracted and sanitized,
///   the sanitized file name is returned as a `String`.
/// - `Err(InvalidGitUrl)`: If the input URL is invalid or does not contain '.git', an [`InvalidGitUrl`]
///   error is returned with an explanatory message.
///
/// # Errors
///
/// The function returns an [`InvalidGitUrl`] error if the input URL does not contain '.git',
/// or if the repository name cannot be extracted from the URL.
///
/// # Examples
///
/// See the unit test of this crate for example use of this private function.
fn git_url_to_filename(url: &str) -> Result<String, InvalidGitUrl> {
    Ok(sanitise_file_name::sanitise(&extract_git_repo_name(url)?))
}

/// Extracts the name of the Git repository from a valid Git URL.
///
/// This function takes a Git URL as input and returns the repository name
/// by removing unnecessary parts such as the protocol and '.git' extension.
/// The name of a Git repository is considered as the host and path of the
/// repository. See examples below.
/// A valid Git URL is expected to contain the '.git' substring.
///
/// # Arguments
///
/// * `url`: A string slice representing the Git URL from which to extract the repository name.
///
/// # Returns
///
/// - `Ok(String)`: If the repository name is successfully extracted, it returns a `String` containing the repository name.
/// - `Err(InvalidGitUrl)`: If the URL does not contain '.git', an [`InvalidGitUrl`] error is returned.
///
/// # Errors
///
/// The function returns an [`InvalidGitUrl`] error if the input URL does not contain '.git'.
///
/// # Examples
///
/// See the unit test of this crate for example use of this private function.
/// Here are some `input -> output` examples:
///
/// * `ssh://user@host.xz:port/path/to/repo.git/path/to/file.txt` -> `host.xz:port/path/to/repo`
/// * `ssh://user@host.xz:port/path/to/repo.git` -> `host.xz:port/path/to/repo`
/// * `ssh://host.xz:port/path/to/repo.git/path/to/file.txt` -> `host.xz:port/path/to/repo`
fn extract_git_repo_name(url: &str) -> Result<String, InvalidGitUrl> {
    // A valid Git URL always has '.git' in it.
    if !url.contains(".git") {
        return Err(InvalidGitUrl("No '.git' found in URL."));
    }

    // In the next lines of code we remove everything from 'url' we did not want.
    // For that we create a mutable String out of 'url'.
    // In the same command, we start removing everything from 'url'
    // starting with '.git'.
    let mut target = url[0..url.find(".git").expect(
        "\
                Unexpected error. \
                We checked the existence of '.git' within the git Git URL before, \
                nevertheless it seems not be present.",
    )]
        .to_string();

    // We identify different separators for the protocol
    // or the user and remove everything from the url from
    // the start reaching to the end of the separator.
    ["@", ":///", "://"].iter().for_each(|sep| {
        if let Some(index) = target.find(sep) {
            target.drain(..index + sep.len());
        }
    });

    Ok(target)
}

#[cfg(test)]
mod tests {
    use crate::git::git_url_to_filename;

    use super::extract_git_repo_name;

    #[test]
    fn invalid_git_urls() {
        let result = extract_git_repo_name("foogit");
        assert!(result.is_err());
        assert_eq!(result.unwrap_err().0, "No '.git' found in URL.");
    }

    #[test]
    fn parse_git_urls() {
        // full ssh URL
        assert_eq!(
            extract_git_repo_name("ssh://user@host.xz:port/path/to/repo.git/path/to/file.txt"),
            Ok("host.xz:port/path/to/repo".to_string())
        );

        // without path to file
        assert_eq!(
            extract_git_repo_name("ssh://user@host.xz:port/path/to/repo.git"),
            Ok("host.xz:port/path/to/repo".to_string())
        );

        // without user
        assert_eq!(
            extract_git_repo_name("ssh://host.xz:port/path/to/repo.git/path/to/file.txt"),
            Ok("host.xz:port/path/to/repo".to_string())
        );

        // without protocol
        assert_eq!(
            extract_git_repo_name("user@host.xz:/path/to/repo.git/path/to/file"),
            Ok("host.xz:/path/to/repo".to_string())
        );

        // different other protocols
        let git_base_url = "host.xy/path/to/repo";
        ["rsync://", "git://", "http://", "https://"]
            .iter()
            .for_each(|protocol| {
                assert_eq!(
                    extract_git_repo_name(&format!("{protocol}{git_base_url}.git")),
                    Ok(git_base_url.to_string())
                )
            });

        // special 'file:///' protocol with three slashes
        assert_eq!(
            extract_git_repo_name("file:///path/to/repo.git"),
            Ok("path/to/repo".to_string())
        );

        // already 'parsed' url without protocol and user
        assert_eq!(
            extract_git_repo_name("host.xz:/path/to/repo.git"),
            Ok("host.xz:/path/to/repo".to_string())
        );
    }

    #[test]
    fn sanitized_url() {
        assert_eq!(
            git_url_to_filename("ssh://user@host.xz:port/path/to/repo.git/path/to/file.txt")
                .unwrap(),
            "host.xz_port_path_to_repo"
        );

        assert_eq!(
            git_url_to_filename("ssh://user@host.xz/~user/path/to/repo.git/").unwrap(),
            "host.xz_~user_path_to_repo"
        );
    }
}

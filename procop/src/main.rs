use procop::config::{DataSource, FileType, SourceType};
use std::vec;

fn main() {
    let source = DataSource {
        source_type: SourceType::Git("git@gitlab.xarif.de:thomass/procop.git".to_string()),
        file_type: FileType::Plain,
    };
    let sources = vec![source];
}

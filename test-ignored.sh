#!/usr/bin/env bash

if [[ $* == *--update-image* ]]
then
  docker build -t procop-test -f ./docker-test-image/Dockerfile .
else
  echo "Run with '--update-image' if you want to update the test Docker image."
fi

if [[ $* == *--keep-container* ]]
then
  DO_RM=""
else
  DO_RM="--rm"
  echo "Run with '--keep-container' if you want to keep the Docker container for later inspection."
fi

if [[ $* == *--rust-backtrace* ]]
then
  RUST_BACKTRACE=1
else
  RUST_BACKTRACE=0
  echo "Run with '--rust-backtrace' if you want to run tests with backtrace."
fi

## define a function for running a single test
function test {
  echo -e "\n\nTest $1"
  echo -e "=====================================================\n"
  docker run \
    $DO_RM \
    -e "RUST_BACKTRACE=$RUST_BACKTRACE" \
    -v "$HOME/.cargo/registry:/usr/local/cargo/registry" \
    -v "$PWD:/usr/src/app" \
    procop-test \
    --package xarif-git \
    --lib \
    -- \
      --show-output \
      --ignored $1
}

## Run each test in a separate container:

test tests::get_repository
test tests::list_branches
test tests::get_updated_repo
FROM alpine/git:latest as git

#
# Test Data 1: A Git Repository with on file and three branches.
#              For the test 'tests::get_repository'
#
RUN mkdir -p /testdata/repo1.git && \
    cd /testdata/repo1.git && \
    git config --global user.name "John Doe" && \
    git config --global user.email johndoe@example.com && \
    git init -b main . && \
    echo "test" > README.md && \
    git add . && \
    git commit -m "test" && \
    git checkout -b branch1 && \
    git checkout -b branch2 && \
    git checkout main

#
# Test Data 2: A copy of repo1 and a clone to the procop workdir.
#              For the test 'tests::list_branches'
#
RUN cp -a /testdata/repo1.git /testdata/repo2.git && \
    git clone /testdata/repo2.git /root/.local/share/procop/testdata_repo2 && \
    cd /testdata/repo2.git && \
    git checkout -b remote1 && \
    git checkout main && \
    cd /root/.local/share/procop/testdata_repo2 && \
    git checkout -b local1 && \
    git checkout branch1 && \
    git checkout main

#
# Test Data 3: New commits in the default branch and a new branch
#
RUN cp -a /testdata/repo1.git /testdata/repo3.git && \
    git clone /testdata/repo3.git /root/.local/share/procop/testdata_repo3 && \
    cd /testdata/repo3.git && \
    echo "bar" > foo.txt && \
    git add . && \
    git commit -m "added foo.txt" && \
    git checkout -b remote1 && \
    echo "mango" > fruits.txt && \
    git add . && \
    git commit -m "added fruits.txt"


FROM rust:1.71.0

COPY --from=git /testdata /testdata
COPY --from=git /root/.local/share/procop /root/.local/share/procop

WORKDIR /usr/src/app
ENTRYPOINT [ "cargo", "test" ]
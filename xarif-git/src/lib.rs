use git2::{Branch, BranchType, ObjectType, Repository};
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

/// Returns an iterator over local branches in a Git repository.
///
/// # Arguments
///
/// * `repo`: A reference to a [`Repository`] object from the [`git2`] crate.
///
/// # Returns
///
/// The function returns an iterator over [`Branch`] objects.
/// The iterator will yield the local branches from the provided repository.
///
/// # Panics
///
/// The function may panic if it fails to retrieve the iterator for the specified branch type. This can occur
/// if the provided `Repository` is not a valid Git repository or if there are other issues accessing Git data.
pub fn local_branches(repo: &Repository) -> impl Iterator<Item = Branch> + '_ {
    branches(repo, BranchType::Local)
}

/// Returns an iterator over remote branches in a Git repository.
///
/// # Arguments
///
/// * `repo`: A reference to a [`Repository`] object from the [`git2`] crate.
///
/// # Returns
///
/// The function returns an iterator over [`Branch`] objects.
/// The iterator will yield the remote branches from the provided repository.
///
/// # Panics
///
/// The function may panic if it fails to retrieve the iterator for the specified branch type. This can occur
/// if the provided `Repository` is not a valid Git repository or if there are other issues accessing Git data.
pub fn remote_branches(repo: &Repository) -> impl Iterator<Item = Branch> + '_ {
    branches(repo, BranchType::Remote)
}

/// Returns an iterator over branches of the specified branch type in a Git repository.
///
/// # Arguments
///
/// * `repo`: A reference to a [`Repository`] object from the [`git2`] crate.
/// * `branch_type`: A [`BranchType`] that specifies the type of branches to iterate over (e.g., [`BranchType::Local`]).
///
/// # Returns
///
/// The function returns an iterator over [`Branch`] objects. The iterator will yield the branches of the specified
/// branch type from the provided repository.
///
/// # Example
///
/// See the unit test of this crate for example use of this private function.
///
/// # Panics
///
/// The function may panic if it fails to retrieve the iterator for the specified branch type. This can occur
/// if the provided `Repository` is not a valid Git repository or if there are other issues accessing Git data.
fn branches(repo: &Repository, branch_type: BranchType) -> impl Iterator<Item = Branch> + '_ {
    repo.branches(Some(branch_type))
        .expect(
            format!(
                "Failed to get the iterator for {:?} branches in {}",
                branch_type,
                repo.path().display()
            )
            .as_str(),
        )
        .map(move |branch| {
            branch
                .expect(
                    format!(
                        "Failed to iterate over the {:?} branches in {}",
                        branch_type,
                        repo.path().display()
                    )
                    .as_str(),
                )
                .0
        })
}

#[derive(Debug)]
pub enum UpdateRepoError {
    RepositoryClone(git2::Error),
    RepositoryOpen(git2::Error),
}

impl UpdateRepoError {
    fn from_repo_clone(err: git2::Error) -> UpdateRepoError {
        UpdateRepoError::RepositoryClone(err)
    }

    fn from_repo_open(err: git2::Error) -> UpdateRepoError {
        UpdateRepoError::RepositoryOpen(err)
    }
}

pub fn update_repository(url: &str, target_path: &Path) -> Result<Repository, UpdateRepoError> {
    match target_path.exists() {
        true => {
            let repo = Repository::open(target_path).map_err(UpdateRepoError::from_repo_open)?;
            fetchall(&repo);
            Ok(repo)
        }
        false => {
            let repo =
                Repository::clone(url, target_path).map_err(UpdateRepoError::from_repo_clone)?;
            Ok(repo)
        }
    }
}

// TODO: testen, ob diese (fertige) funktion funkioniert
pub fn fetchall(repo: &Repository) {
    repo.remotes()
        .expect("Expect to get remotes.")
        .iter()
        .for_each(|i| {
            let mut remote = repo
                .find_remote(i.expect("Expected Remote name."))
                .expect("Exptected remote.");

            let mut fetch_options = git2::FetchOptions::new();
            fetch_options.remote_callbacks(get_remote_callbacks());

            remote
                .fetch_refspecs()
                .expect("Expected to fetch refspecs.")
                .iter()
                .for_each(|refspec| {
                    remote
                        .fetch(
                            &[refspec.expect("Expected valid refspec.")],
                            Some(&mut fetch_options),
                            None,
                        )
                        .expect(format!("Expected to fetch refspec '{:?}'", refspec).as_str());
                });
        });
}

pub fn prune_local_branches(repo: &Repository) {
    // TODO: Vorgehen:
    // * alle local_branhches Branches ohne upstream löschen
    // * alle local_branches Branches mit upstream, welches in remote_branches nicht mehr vorhandenn ist, löschen

    let err_no_name = "Unable to get the name of a branch.";
    let err_invalid_utf8 = "A branch name is not valid utf-8.";

    let mut remote_branch_names = remote_branches(repo).map(|b| {
        b.name()
            .expect(err_no_name)
            .expect(err_invalid_utf8)
            .to_string()
    });

    local_branches(repo).for_each(|mut lb| {
        if !remote_branch_names.any(|rb| {
            rb == lb
                .name()
                .expect(err_no_name)
                .expect(err_invalid_utf8)
                .to_string()
        }) {
            lb.delete().expect("Failed to delete local branch.");
        }
    })
}

fn get_remote_callbacks() -> git2::RemoteCallbacks<'static> {
    let mut callbacks = git2::RemoteCallbacks::new();

    callbacks.credentials(|_url, username_from_url, _allowed_types| {
        let username = match username_from_url {
            Some(username) => username,
            None => panic!("Could not get username. This is a bug"),
        };
        git2::Cred::ssh_key_from_agent(username)
    });

    callbacks
}

pub fn clone_file_from_repository(
    url: &str,
    file_path: &str,
    target_path: &str,
) -> std::io::Result<()> {
    let repository = Repository::clone(url, target_path).unwrap();

    // Resolve the file's Oid (object ID) from its path
    let oid = repository
        .revparse_single(&format!("HEAD:{}", file_path))
        .unwrap()
        .id();

    // Get the object representing the file
    let object = repository.find_object(oid, Some(ObjectType::Blob)).unwrap();

    // Read the file content as bytes
    let file_content = object.as_blob().unwrap().content();

    // Write the content to a local file
    let mut file = File::create(file_path)?;
    file.write_all(file_content).unwrap();

    Ok(())
}

#[cfg(test)]
mod tests {

    use super::{local_branches, remote_branches, update_repository};

    #[test]
    #[ignore]
    /// This test is meant to be run with the './test-ignored.sh' script!
    ///
    /// It's test data is 'Test Data 1' from the docker-test-image.
    fn get_repository() {
        let path_to_remote_repo = "/testdata/repo1.git";
        let path_to_local_repo = std::path::Path::new("/root/.local/share/procop/testdata_repo1");
        let path_to_readme_in_local_repo = path_to_local_repo.join("README.md");

        // Check that the target path does NOT exist
        assert!(!path_to_readme_in_local_repo.exists());

        let repo1 = update_repository(path_to_remote_repo, path_to_local_repo)
            .expect("Expected not to fail.");

        // Test that we have cloned the repo to the new target
        assert!(path_to_readme_in_local_repo.exists());
        assert!(path_to_readme_in_local_repo.is_file());

        // Modify the Git repo, such that it differs from the original one, by creating a tag
        assert!(repo1.tag_names(None).unwrap().is_empty());
        let _ = repo1.tag_lightweight(
            "foo",
            repo1.head().unwrap().peel_to_commit().unwrap().as_object(),
            false,
        );
        assert_eq!(
            repo1
                .tag_names(None)
                .expect("Expected a list of Git tags.")
                .get(0)
                .expect("Expected the first Git tag 'foo' exists."),
            "foo"
        );

        // Assert that repo2 is not cloned again, but points to the same repo as repo1.
        // We identify this by having the tag 'foo'.
        let repo2 = update_repository(path_to_remote_repo, path_to_local_repo)
            .expect("Expected not to fail.");
        assert_eq!(
            repo2
                .tag_names(None)
                .expect("Expected a list of Git tags.")
                .get(0)
                .expect("Expected the first Git tag 'foo' exists."),
            "foo"
        );

        // remove the target repository
        let _ = std::fs::remove_dir_all("/root/.local/share/procop/testdata_repo1")
            .expect("Failed to remove '/root/.local/share/procop/testdata_repo1'.");
        // Check that the target path does NOT exist
        assert!(!path_to_readme_in_local_repo.exists());

        // Assert that repo3 is cloned again.
        // We identify this by not having the tag 'foo' from repo1 and repo2.
        let repo3 = update_repository(path_to_remote_repo, path_to_local_repo)
            .expect("Expected not to fail.");
        assert!(repo3
            .tag_names(None)
            .expect("Expected an (empty) list of Git tags.")
            .is_empty());
    }

    #[test]
    #[ignore]
    /// This test is meant to be run with the './test-ignored.sh' script!
    ///
    /// It's test data is 'Test Data 2' from the docker-test-image.
    fn list_branches() {
        let repo = git2::Repository::open("/root/.local/share/procop/testdata_repo2")
            .expect("Failed to open repository at /root/.local/share/procop/testdata_repo2");

        // print branches for visual test output
        local_branches(&repo).for_each(|b| {
            if let Ok(upstream) = b.upstream() {
                println!("{:?}", upstream.name())
            }
        });

        let mut local_branches = local_branches(&repo);
        let mut remote_branches = remote_branches(&repo);

        assert_eq!(local_branches.next().unwrap().name(), Ok(Some("branch1")));
        // Attention: The remote branch 'branch2' is not checked out locally.
        assert_eq!(local_branches.next().unwrap().name(), Ok(Some("local1")));
        assert_eq!(local_branches.next().unwrap().name(), Ok(Some("main")));
        assert!(local_branches.next().is_none());

        assert_eq!(
            remote_branches.next().unwrap().name(),
            Ok(Some("origin/HEAD"))
        );
        assert_eq!(
            remote_branches.next().unwrap().name(),
            Ok(Some("origin/branch1"))
        );
        assert_eq!(
            remote_branches.next().unwrap().name(),
            Ok(Some("origin/branch2"))
        );
        // Attention: There is a remote branch 'remote1', but
        // this branch isn't fetched yet.
        assert_eq!(
            remote_branches.next().unwrap().name(),
            Ok(Some("origin/main"))
        );
        assert!(remote_branches.next().is_none());
    }

    #[test]
    #[ignore]
    fn get_updated_repo() {
        let path_to_remote_repo = "/testdata/repo3.git";
        let path_to_local_repo = std::path::Path::new("/root/.local/share/procop/testdata_repo3");
        let path_to_foo_txt_in_local_repo = path_to_local_repo.join("foo.txt");

        assert!(path_to_local_repo.exists());
        assert!(!path_to_foo_txt_in_local_repo.exists());

        update_repository(path_to_remote_repo, path_to_local_repo).expect("Expected not to fail.");

        assert!(path_to_foo_txt_in_local_repo.exists());
    }
}
